var bg;
var dinos = [];
var N = 10;
var cont =0;
var cont_frames = 0;
var delay = 5;

function setup() {
    bg = loadImage('imgs/jurassic.png');
    console.log(bg.width);
    createCanvas(400,200);
    
    for(let i = 1; i <= N; i++) {
        dinos.push(loadImage("imgs/Walk ("+i+").png"))
    }
}
  
function draw() {
    cont_frames++;
    
    if (!(cont_frames % delay) ){
        background(bg);

        let img = dinos[cont];
        image(img,cont_frames,80,img.width/4,img.height/4);
    
        cont++;
        if(cont >= N) {
            cont = 0;
        }
        if(cont_frames >= 400) {
            cont_frames = 0;
        }
    }

}